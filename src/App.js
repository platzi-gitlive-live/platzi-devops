import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p><h1> TITULO DE LA APLICACION</h1></p>

        <p>
          Hello this is a new text: landing paragraph
        </p>
        <p>
          Hola este es un nuevo texto : pagina de bienvenida
        </p>

        <p><h1>texto inferior</h1></p>

        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
